import React from "react";
import { Jumbotron, Button, Container }from "react-bootstrap";

const JumbotronComponent = () => {
  return (
    <div className="text-center">
      <Jumbotron fluid>
        <Container>
          <h1 className="py-5 mt-5">PLAY TRADITIONAL GAME</h1>
          <p className="py-5 my-5">Experience New Traditional Game</p>
          <p>
            <Button className="px-4 py-3" variant="danger">PLAY NOW</Button>
          </p>
        </Container>
      </Jumbotron>
    </div>
  );
};

export default JumbotronComponent;

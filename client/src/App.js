import Landing from './views/Landing';
// import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <Landing />
    </div>
  );
}

export default App;

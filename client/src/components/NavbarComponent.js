import React from "react";
import {
  Nav,
  Navbar,
  NavDropdown,
  Form,
  Button,
  FormControl,
} from "react-bootstrap";

const NavbarComponent = () => {
  return (
    <Navbar bg="secondary" expand="lg">
      <Navbar.Brand href="#home">
        TEAM <strong>O.N.E</strong>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="#home">Home</Nav.Link>
          <Nav.Link href="#link">Leaderboards</Nav.Link>
          <NavDropdown title="Our Games" id="basic-nav-dropdown">
            <NavDropdown.Item href="#action/3.1">Games 1</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.2">Games 2</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.3">Games 3</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.4">Games 4</NavDropdown.Item>
          </NavDropdown>
        </Nav>
        <Nav className="ml-auto">
          <Form inline>
            <FormControl type="text" placeholder="Search" className="ml-sm-2" />
            <Button variant="outline-light">Search</Button>
          </Form>
          <Nav.Link href="#login">Login</Nav.Link>
          <Nav.Link href="#register">Register</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default NavbarComponent;

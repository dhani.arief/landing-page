import React, { Component } from 'react'
import Footer from '../components/Footer'
import JumbotronComponent from '../components/JumbotronComponent'
import NavbarComponent from '../components/NavbarComponent'
import '../styles/Landing.css'

export default class Landing extends Component {
    render() {
        return (
            <div>
                <NavbarComponent />
                <JumbotronComponent />
                <Footer />
            </div>
        )
    }
}
